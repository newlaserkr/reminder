package com.example.reminder.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.reminder.R

import com.example.reminder.model.SoundModel
import kotlinx.android.synthetic.main.individual_sound_tab.view.*
import java.util.logging.Logger


class SoundsAdapter(var soundsList: List<SoundModel>, val onSoundListener: OnSoundListener) :
    RecyclerView.Adapter<SoundsAdapter.ReminderViewHolder>() {

    lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReminderViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        context = parent.context

          val view : View = inflater.inflate(R.layout.individual_sound_tab, parent, false)
            return ReminderViewHolder(view)

    }

    override fun getItemCount() = soundsList.size

    override fun onBindViewHolder(holder: ReminderViewHolder, position: Int) {

        holder.view.name_of_song_tv.text = soundsList[position].soundTitle
        holder.view.selection_rv.setOnClickListener{
            for (soundModel:SoundModel in soundsList){
                soundModel.soundSelected = false
            }
            soundsList[position].soundSelected = true
            onSoundListener.onSongPlay(soundsList[position])
        }

        holder.view.radio_bt.setOnClickListener{
            for (soundModel:SoundModel in soundsList){
                soundModel.soundSelected = false
            }
            soundsList[position].soundSelected = true
            onSoundListener.onSongPlay(soundsList[position])
        }

        if (soundsList[position].soundSelected!! == true){
            holder.view.radio_bt.setChecked(true)
            holder.view.selection_rv.setBackgroundColor(context.getResources().getColor(R.color.darkGray50))
//            onSoundListener.onSongPlay(soundsList[position])
        }else {
            holder.view.radio_bt.setChecked(false)
            holder.view.selection_rv.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary))
        }
        Log.e("NOTGOOD", "Boolean is true : " + position)
    }

    class ReminderViewHolder(var view: View) : RecyclerView.ViewHolder(view)

    fun updateAdapter(soundsList: List<SoundModel>) {
        this.soundsList = soundsList
        notifyDataSetChanged()
    }

    interface OnSoundListener{
        fun onSongPlay(model: SoundModel)
    }
}