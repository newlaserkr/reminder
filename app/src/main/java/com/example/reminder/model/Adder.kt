package com.example.reminder.model

import android.graphics.drawable.GradientDrawable

data class Adder(
    val adderId : String?,
    val adderTitle : String?,
    val adderIcon : Int,
    val background : GradientDrawable,
    val color : Int
)