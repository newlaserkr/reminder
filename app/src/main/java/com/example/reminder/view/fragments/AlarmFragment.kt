package com.example.reminder.view.fragments


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.reminder.R
import kotlinx.android.synthetic.main.fragment_reminder.*

class AlarmFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_alarm, container, false)

    override fun onDestroyView() {
        super.onDestroyView()
        System.out.println("OnDestroyView Alarm")
    }

}
