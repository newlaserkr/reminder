package com.example.reminder.view.fragments

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders

import com.example.reminder.R
import com.example.reminder.viewmodel.WakeupViewModel
import kotlinx.android.synthetic.main.fragment_wakeup.*
import javax.inject.Inject


class WakeupFragment : Fragment() {

    lateinit var viewModel : WakeupViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Log.e("tag", "We are here that's for sure")

        activity?.let {
            viewModel = ViewModelProviders.of(it).get(WakeupViewModel::class.java)
        }

        viewModel.initialization(this.activity!!.intent.getStringExtra("extra"))

        this.activity!!.window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN or
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
            WindowManager.LayoutParams.FLAG_FULLSCREEN or
                    WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED or
                    WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON)

//        pulsator.start()
//        stop_wakeup_bt.setOnClickListener {
//            val intent = Intent(this.activity, MainActivity::class.java)
//            startActivity(intent)
//        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(R.layout.fragment_wakeup, container, false)
        observeViewModelReminder()

        return view
    }

    fun observeViewModelReminder() {
        viewModel.reminder.observe(this, Observer { reminder ->
            reminder?.let {
                val animation = AnimationUtils.loadAnimation(this.activity, R.anim.pulsation)
                reminder_iv.setImageResource(reminder.reminderIcon)
                main_text_tv.text = getString(R.string.exercise_time)
                reminder_tv.text = reminder.reminderTitle + " / 12.02.2020 14:00"
                stop_wakeup_bt.setColorFilter(ContextCompat.getColor(this.activity!!, reminder.reminderIconColor));
                stop_wakeup_bt.startAnimation(animation)
                ripple_bg.startRippleAnimation()
                viewModel.playTone()
            }
        })
    }


}
