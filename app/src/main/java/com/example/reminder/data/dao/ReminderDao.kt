package com.example.reminder.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.reminder.data.entity.Reminder

@Dao
interface ReminderDao {

    @Insert
    suspend fun insertAll(vararg reminder: Reminder): MutableList<Long>

    @Insert
    suspend fun insertOne(vararg reminder: Reminder)

    @Query("SELECT * FROM reminder")
    suspend fun getAllReminders(): MutableList<Reminder>

    @Query("SELECT * FROM reminder WHERE unique_identifier = :remId")
    suspend fun getReminder(remId : String?): Reminder

    @Query("DELETE FROM reminder")
    suspend fun deleteAll()
}