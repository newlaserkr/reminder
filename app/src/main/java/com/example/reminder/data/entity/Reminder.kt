package com.example.reminder.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Reminder(

    @ColumnInfo(name = "unique_identifier")
    val uniqueId : String,

    @ColumnInfo(name = "reminder_title")
    val reminderTitle : String?,

    @ColumnInfo(name = "reminder_date")
    val reminderDate : String?,

    @ColumnInfo(name = "reminder_time")
    val reminderTime : String?,

    @ColumnInfo(name = "reminder_repeat")
    val reminderRepeat : String?,

    @ColumnInfo(name = "reminder_interval")
    val reminderInterval : String?,

    @ColumnInfo(name = "reminder_song_path")
    val reminderSong : String?,

    @ColumnInfo(name = "reminder_type")
    val reminderType : String?,

    @ColumnInfo(name = "reminder_tag")
    val reminderTag : String?,

    @ColumnInfo(name = "reminder_icon")
    val reminderIcon : Int,

    @ColumnInfo(name = "reminder_icon_color")
    val reminderIconColor : Int,

    @ColumnInfo(name = "reminder_icon_type")
    val reminderTypeIcon : Int
){
    @ColumnInfo(name = "reminder_id")
    @PrimaryKey(autoGenerate = true)
    var reminderId : Int = 0
}