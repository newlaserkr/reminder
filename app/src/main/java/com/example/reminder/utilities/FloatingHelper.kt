package com.example.reminder.utilities

import android.content.Context
import android.view.View
import android.view.animation.AnimationUtils
import com.example.reminder.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import android.view.animation.Animation
import android.widget.FrameLayout
import android.widget.TextView


class FloatingHelper(
    val floatingMain: FloatingActionButton,
    val floatingSecond: FloatingActionButton,
    val floatingThird: FloatingActionButton,
    val repeatingTv: TextView,
    val onetimeTv: TextView,
    val floating_background: FrameLayout
) {

    var fab_open: Animation? = null
    var fab_close: Animation? = null
    var fab_clock: Animation? = null
    var fab_anticlock: Animation? = null
    var isOpen = false


    fun setListener(context: Context, floatingInterface: FloatingActionListener) {

        fab_close = AnimationUtils.loadAnimation(context, R.anim.fab_close);
        fab_open = AnimationUtils.loadAnimation(context, R.anim.fab_open);
        fab_clock = AnimationUtils.loadAnimation(context, R.anim.fab_rotate_c);
        fab_anticlock = AnimationUtils.loadAnimation(context, R.anim.fab_rotate_ac);

        floatingMain.setOnClickListener {
            if (isOpen) {
                floatingInterface.showHideBackground(false)
                closeFloating()
            } else {
                floatingInterface.showHideBackground(true)
                openFloating()
            }
        }

        floatingSecond.setOnClickListener {
            floatingInterface.repeating()
        }

        floatingThird.setOnClickListener {
            floatingInterface.oneTime()
        }
    }

    fun closeFloating(){
        floating_background.setVisibility(View.INVISIBLE)
        repeatingTv.setVisibility(View.INVISIBLE)
        onetimeTv.setVisibility(View.INVISIBLE)
        floatingSecond.startAnimation(fab_close)
        floatingThird.startAnimation(fab_close)
        floatingMain.startAnimation(fab_anticlock)
        floatingSecond.setClickable(false)
        floatingThird.setClickable(false)
        isOpen = false;
    }

    fun openFloating(){
        floating_background.setVisibility(View.VISIBLE)
        repeatingTv.setVisibility(View.VISIBLE)
        onetimeTv.setVisibility(View.VISIBLE)
        floatingSecond.startAnimation(fab_open)
        floatingThird.startAnimation(fab_open)
        floatingMain.startAnimation(fab_clock)
        floatingSecond.setClickable(true)
        floatingThird.setClickable(true)
        isOpen = true
    }

    interface FloatingActionListener {
        fun showHideBackground(variable : Boolean)
        fun oneTime()
        fun repeating()

    }
}