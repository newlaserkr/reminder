package com.example.reminder.utilities

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.example.reminder.R
import com.example.reminder.view.WakeupActivity


class Receiver : BroadcastReceiver() {
    private val CHANNEL_ID = "personal_notification"
    private var notificationManager: NotificationManager? = null

    override fun onReceive(context: Context?, intent: Intent?) {

        val bundle = intent!!.getStringExtra("extra")

//        Log.e("tag","Received :"+" ----------------------------------------------------------------------------------------------------------")
//        val i = Intent(context, WakeupActivity::class.java)
//        Log.e("tag","Received :" +" ----------------------------------------------------------------------------------------------------------")
//        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
//        Log.e("tag","Received :" +" ----------------------------------------------------------------------------------------------------------")
////        i.putExtra("extra",bundle)
//        Log.e("tag","Received :" +" ----------------------------------------------------------------------------------------------------------")
//        context!!.startActivity(i)
        Log.e("tag","Received :" +" ----------------------------------------------------------------------------------------------------------")
        notificationManager = context!!.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notificationBuilder = NotificationCompat.Builder(context, bundle)
        val fullScreenIntent = Intent(context, WakeupActivity::class.java)
        fullScreenIntent.putExtra("extra",bundle)
        val fullScreenPendingIntent = PendingIntent.getActivity(context, 0,
            fullScreenIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(bundle, "Reminder", NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.lightColor = Color.RED
            notificationManager!!.createNotificationChannel(notificationChannel)
            notificationBuilder
                .setSmallIcon(R.drawable.ic_date)
                .setContentTitle("Reminder")
                .setContentText("Something")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setFullScreenIntent(fullScreenPendingIntent, true)
        }else {
            notificationBuilder
                .setSmallIcon(R.drawable.ic_date)
                .setContentTitle("Reminder")
                .setContentText("Something")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setFullScreenIntent(fullScreenPendingIntent, true)
        }
        Log.e("tag","Received :" +" ----------------------------------------------------------------------------------------------------------")

        notificationManager!!.notify(1,notificationBuilder.build())
    }
}